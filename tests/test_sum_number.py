from src.main import sum_number


def test_sum_function():
    result = sum_number(2, 5)
    assert 7 == result
